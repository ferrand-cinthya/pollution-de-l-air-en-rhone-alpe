import psycopg2
from psycopg2.extensions import parse_dsn






def Creation_table():
    db_dsn = "postgres://postgres:test@localhost:5432/decouverte"
    db_args = parse_dsn(db_dsn)
    conn = psycopg2.connect(**db_args)
    cur = conn.cursor()

    Commande_Drop = "DROP TABLE IF EXISTS mesure"
    cur.execute(Commande_Drop)


    ######## CREATION TABLE data_brute_covid ########

    cur.execute('''CREATE TABLE IF NOT EXISTS data_brute_covid(
    nom_dept VARCHAR(256),
    nom_com VARCHAR(256),
    insee_com VARCHAR(128),
    nom_station VARCHAR(256),
    code_station VARCHAR(256),
    typologie VARCHAR(256),
    influence VARCHAR(256),
    polluant_court VARCHAR(256),
    id_poll_ue INTEGER,
    valeur FLOAT8,
    unite VARCHAR(256),
    date_debut TIMESTAMP WITH TIME ZONE,
    date_fin TIMESTAMP WITH TIME ZONE,
    validite BOOLEAN,
    x_wgs84 FLOAT8,
    y_wgs84 FLOAT8,UNIQUE(code_station,date_debut,id_poll_ue))''')


    ######## CREATION TABLE data_brute_polluant ########

    cur.execute('''CREATE TABLE IF NOT EXISTS data_brute_polluant(
    nom_dept VARCHAR(256),
    nom_com VARCHAR(256),
    insee_com VARCHAR(128),
    nom_station VARCHAR(256),
    code_station VARCHAR(256),
    typologie VARCHAR(256),
    influence VARCHAR(256),
    nom_poll VARCHAR(256),
    id_poll_ue INTEGER,
    valeur FLOAT8,
    unite VARCHAR(256),
    date_debut TIMESTAMP WITH TIME ZONE,
    date_fin TIMESTAMP WITH TIME ZONE,
    statut_valid BOOLEAN,
    x_wgs84 FLOAT8,
    y_wgs84 FLOAT8,UNIQUE(code_station,date_debut,id_poll_ue))''')


    ######## CREATION TABLE department ########


    cur.execute('''CREATE TABLE IF NOT EXISTS departement(
    "pkid" SERIAL,
    "nom_departement" VARCHAR(256),
    PRIMARY KEY("pkid"))''')


    ######## CREATION TABLE commune ########

    cur.execute('''CREATE TABLE IF NOT EXISTS commune(
    "pkid" SERIAL,
    "nom_commune" VARCHAR(256),
    "insee" VARCHAR(256),
    PRIMARY KEY("pkid"))''')

    ######## CREATION TABLE polluant ########


    cur.execute('''CREATE TABLE IF NOT EXISTS polluant(
    "pkid" SERIAL,
    "identifiant_polluant" INTEGER,
    "nom_polluant" VARCHAR(256),
    "unite" VARCHAR(256),
    PRIMARY KEY("pkid"))''')


    ######## CREATION TABLE station ########

    cur.execute('''CREATE TABLE IF NOT EXISTS station (
    "pkid" SERIAL,
    "code_station" VARCHAR(256),
    "nom_station" VARCHAR(256),
    x_wgs84 FLOAT8,
    y_wgs84 FLOAT8,
    PRIMARY KEY("pkid"))''')

    ######## CREATION TABLE mesure ########

    cur.execute('''CREATE TABLE IF NOT EXISTS mesure (
    id_nom_departement INTEGER,
    id_nom_commune INTEGER,
    id_code_station INTEGER,
    id_polluant INTEGER,
    concentration_polluant FLOAT8,
    date_debut TIMESTAMP,
    date_fin TIMESTAMP,
    typologie VARCHAR(100),
    influence VARCHAR(100),
    statut_valid BOOLEAN,    
    FOREIGN KEY ("id_nom_departement") REFERENCES departement ("pkid"),
    FOREIGN KEY ("id_nom_commune") REFERENCES commune ("pkid"),
    FOREIGN KEY ("id_polluant") REFERENCES polluant ("pkid"),
    FOREIGN KEY ("id_code_station") REFERENCES station ("pkid"))''')

    conn.commit()
    cur.close()
    conn.close()
    return








