import psycopg2
from psycopg2.extensions import parse_dsn
import requests

######### Interroger Base de Données #########

PM10 = requests.get('https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_4.geojson')
N02 = requests.get('https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_1.geojson')
COVID_N02 = requests.get('https://opendata.arcgis.com/datasets/5e828516bbd44aa9aefd1443aac8239f_1.geojson')
COVID_PM10 = requests.get('https://opendata.arcgis.com/datasets/5e828516bbd44aa9aefd1443aac8239f_4.geojson')
Json_pm10 = PM10.json()
JSON_N02 = N02.json()
COVID19_PM10 = COVID_PM10.json()
COVID19_N02 = COVID_N02.json()

Polluant_Nom = [Json_pm10, JSON_N02]
Covid_Polluant = [COVID19_PM10,COVID19_N02]

Nom_Colonne = ["nom_dept", "nom_com", "insee_com", "nom_station", "code_station", "typologie", "influence", "nom_poll",
               "id_poll_ue", "valeur", "unite", "date_debut", "date_fin", "statut_valid", "x_wgs84", "y_wgs84"]

Nom_Colonne_Covid = ["nom_dept", "nom_com", "insee_com", "nom_station", "code_station", "typologie", "influence", "polluant_court",
               "id_poll_ue", "valeur", "unite", "date_debut", "date_fin", "validite", "x_wgs84", "y_wgs84"]

######### Connection PostgreSQL , Parcourir donnees et Tranfert dans la table #########

def Transfert_sql():
    db_dsn = "postgres://postgres:test@localhost:5432/decouverte"
    db_args = parse_dsn(db_dsn)
    conn = psycopg2.connect(**db_args)
    cur = conn.cursor()
    for VALEUR in Polluant_Nom:
        for Polluant_valeur in VALEUR["features"]:
            Donnees_polluant = []
            for cle, value in Polluant_valeur["properties"].items():
                if cle in Nom_Colonne:
                    Donnees_polluant.append(value)
                    requete = "INSERT INTO data_brute_polluant VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) ON CONFLICT (code_station,date_debut,id_poll_ue) DO NOTHING"
            cur.execute(requete,Donnees_polluant)

    for VALEUR in Covid_Polluant:
        for Polluant_valeur in VALEUR["features"]:
            Donnees_polluant_Covid = []
            for cle, value in Polluant_valeur["properties"].items():
                if cle in Nom_Colonne_Covid:
                    Donnees_polluant_Covid.append(value)
                    requete = "INSERT INTO data_brute_covid VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) ON CONFLICT (code_station,date_debut,id_poll_ue) DO NOTHING"
            cur.execute(requete,Donnees_polluant_Covid)

    conn.commit()  # Validation pour integriter des données
    cur.close()
    conn.close()
    return

