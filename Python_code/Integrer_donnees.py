import psycopg2
from psycopg2.extensions import parse_dsn


def integration_donnees():
    db_dsn = "postgres://postgres:test@localhost:5432/decouverte"
    db_args = parse_dsn(db_dsn)
    conn = psycopg2.connect(**db_args)
    cur = conn.cursor()

    ######## Integration des polluants,id,unite UNIQUE ########

    cur.execute("INSERT INTO polluant (identifiant_polluant,nom_polluant,unite)"
                " select distinct id_poll_ue,nom_poll,unite from data_brute_polluant where not exists"
                " (select identifiant_polluant from polluant where identifiant_polluant = data_brute_polluant.id_poll_ue)")

    ######## Integration DES DEPARTEMENTS,id,department UNIQUE ########

    cur.execute("INSERT INTO departement (nom_departement)"
                " select distinct nom_dept from data_brute_polluant where not exists"
                " (select nom_departement from departement where nom_departement = data_brute_polluant.nom_dept)")

    ######## Integration DES COMMUNE,id,nom_commune,insee UNIQUE ########

    cur.execute("INSERT INTO commune (nom_commune,insee)"
                " select distinct nom_com,insee_com from data_brute_polluant where not exists"
                " (select nom_commune from commune where nom_commune = data_brute_polluant.nom_com)")

    ######## Integration DES STATION,id,department UNIQUE ########

    cur.execute("INSERT INTO station (code_station,nom_station,x_wgs84,y_wgs84)"
                " select distinct code_station,nom_station,x_wgs84,y_wgs84 from data_brute_polluant where not exists"
                " (select code_station from station where code_station = data_brute_polluant.code_station)")


    cur.execute("INSERT INTO mesure (id_nom_departement,id_nom_commune,id_code_station,id_polluant,concentration_polluant,date_debut,date_fin,typologie,influence,statut_valid) "
                "SELECT departement.pkid,commune.pkid,station.pkid,polluant.pkid,valeur,date_debut,date_fin,typologie,influence,statut_valid "
                "FROM data_brute_polluant "
                "JOIN departement ON data_brute_polluant.nom_dept = departement.nom_departement "
                "JOIN commune ON data_brute_polluant.nom_com = commune.nom_commune "
                "JOIN station ON data_brute_polluant.nom_station = station.nom_station "
                "JOIN polluant ON data_brute_polluant.nom_poll = polluant.nom_polluant")
    conn.commit()
    cur.close()
    conn.close()

    return




